class CreateItems < ActiveRecord::Migration[7.0]
  def change
    create_table :items, id: :uuid do |t|
      t.references :category,  null: false, foreign_key: true
      t.string :name
      t.decimal :price, precision: 8, scale: 2
      t.datetime :deleted_at
      t.timestamps
    end

    add_index :items, :name
  end
end
