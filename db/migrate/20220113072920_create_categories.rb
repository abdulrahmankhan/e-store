class CreateCategories < ActiveRecord::Migration[7.0]
  def change
    create_table :categories do |t|
      t.string :key
      t.datetime :deleted_at
      t.timestamps
    end
    add_index :categories, :key, :unique => true
  end
end
