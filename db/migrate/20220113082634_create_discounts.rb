class CreateDiscounts < ActiveRecord::Migration[7.0]
  def change
    create_table :discounts, id: :uuid do |t|
      t.string :title
      t.references :category, null: false, foreign_key: true
      t.string :discount_key
      t.string :discount_type #Enum
      t.integer :percentage
      t.integer :base_items
      t.integer :free_items
      t.datetime :active_from
      t.datetime :active_till
      t.datetime :deleted_at
      t.timestamps
    end
    add_index :discounts, :title
  end
end
