# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
ActiveRecord::Base.transaction do
    # Insert Categories
    categoriesData = [
        {
            key: 'MUG',
        },
        {
            key: 'TSHIRT'
        },
        {
            key: "HOODIE"
        }
    ]
    categories = Category.create(categoriesData)

    # Insert Items

    itemsData = [
        {
            name: 'Reedsy Mug',
            price: 6.00,
            category_id: Category.where(key: 'MUG').first.id
        },
        {
            name: 'Reedsy T-shirt',
            price: 15.00,
            category_id: Category.where(key: 'TSHIRT').first.id
        },
        {
            name: 'Reedsy Hoodie',
            price: 20.00,
            category_id: Category.where(key: 'HOODIE').first.id
        }
    ]
    item = Item.create(itemsData)

    # Insert Discounts 
    # Note: Using this method because enum wasn't populating with bulk action.
    discount = Discount.new
    discount.title = '2-for-1 (buy two, one of them is free) for the MUG item'
    discount.discount_type = "free_item"
    discount.discount_key = ApplicationHelper::generate_discount_key
    discount.category_id = Category.where(key: 'MUG').first.id
    discount.base_items = 2
    discount.free_items = 1
    discount.save

    discount = Discount.new
    discount.title = '30% discounts on all TSHIRT items when buying 3 or more'
    discount.discount_type = "percentage"
    discount.discount_key = ApplicationHelper::generate_discount_key
    discount.category_id = Category.where(key: 'TSHIRT').first.id
    discount.base_items = 2
    discount.percentage = 30
    discount.save


end