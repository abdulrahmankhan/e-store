module Api::ItemsHelper


    def self.discounted_price(items, price_data)
        price_total = 0.00
        # fetch unique categories
        # Check if each category has any discount against it. 
        category_ids = items.distinct.pluck(:category_id)
        categories = Category.where(id: category_ids)
        categories.each do |category|
            category_items = items.where(category_id: category.id)
            discount = category.discounts.first
            items_count = count_items(category_items, price_data)

            if discount
                if items_count > discount.base_items
                    if discount.discount_type == 'percentage'
                        price_total += (category_items.first.price * items_count) - ((category_items.first.price * items_count) * (discount.percentage / 100.0))
                    elsif discount.discount_type == 'free_item'
                        # price_total += (category_items.first.price * (items_count - 1))
                    end
                else
                    price_total  += category_items.first.price * items_count
                end
            else
                price_total  += category_items.first.price * items_count
            end
        end
       
        price_total
    end
    def self.percentage

    end
    def self.count_items(category_items, price_data)
        total_items = 0
        category_items.each do |category_item|
            total_items += price_data[category_item.id]
        end
        total_items
    end

end
