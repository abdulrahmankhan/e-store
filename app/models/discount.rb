class Discount < ActiveRecord::Base
    validates :title, presence: true
    validates :discount_type, presence: true
    validates :discount_key, presence: true
    belongs_to :category
    enum type: [:percentage, :free_item]
end
