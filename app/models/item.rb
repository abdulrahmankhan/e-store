class Item < ActiveRecord::Base
    belongs_to :category
    validates :name, presence: true
    validates :price, presence: true
    

    def format_price()
        ActionController::Base.helpers.number_to_currency(price, unit: '€')
    end
end
