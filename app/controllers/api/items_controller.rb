class Api::ItemsController < Api::ApiController

    def index
        @items = Item.all
    end

    def show
        @item = Item.find(params[:id])
    end

    def update
        price = update_params
        @item = Item.find(params[:id])
        @item.price = price.to_i
        @item.save
        @item
    end
    def price
        # Sample data {"0c7157c5-c68d-4cf7-baeb-3bdb605ddf54":1,"21e9972c-fde5-45d5-92cc-cdfbd800ee04":3}
        price_data = params[:items]
        @items = Item.where(id: price_data.keys)
        render json: Api::ItemsHelper::discounted_price(@items, price_data)
        # return render json: price_data
        # fetch uuids 
        # fetch items
        # discount = Discount.where(discount_key: discount_code).first
        # if discount

        # end
        # uuids = params[:uuids]
        # price = Item.where(id: uuids).sum(:price)     
        # render json: price
        # render json: 
    end

    private
    def update_params
        params.require(:price)
    end

    def price_params
        params.require(:ids)
    end
end
